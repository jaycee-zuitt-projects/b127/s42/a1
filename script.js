let items = document.getElementsByClassName("colorMe");
// console.log(items);


for(const item of items){
    item.addEventListener("click", () => {
        let color = prompt("Enter Color Name");
        item.style.backgroundColor = color;
        item.innerHTML = color
    });
}